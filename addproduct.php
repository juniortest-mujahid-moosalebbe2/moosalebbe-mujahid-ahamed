<?php
    include 'connection.php';
?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="add.css"><!-- stylesheet -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>    
    <title>Product Add</title>
    <style type="text/css">
        /*.dvd{
            display: none;
        }*/
    </style>
</head>
<body>
    <div class="mujahid-ahamed-test-wrapper"> 
        <div class="mujahid-ahamed"> 
            <div class="mujahid-ahamed-head">
                <div class="mujahid-ahamed-head-heading">
                    <h1>Product Add</h1>
                </div>
                <div class="mujahid-ahamed-button">
                    <div class="mujahid-ahamed-button1">
                        <form method="post">
                            <input type="submit" value="Save" class="save" name="Save">
                            <input type="submit" value="Cancel" class="mass-delete" id="delete-product-btn" name="Cancel">         
                    </div>
                </div>
            </div>
            <hr>
            <form id="product_form" method="post">
                <label><p class="p">SKU</p></label>           
                <input type="text" name="sku" id="sku" ><br><br>
                <label><p class="p">Name</p></label>                            
                <input type="text" name="name" id="name"><br><br>
                <label><p class="p">Price ($)</p></label>
                <input type="text" name="price" id="price"><br><br><br>
                <label>Type Switcher</label>
                <select name="producttype" id="productType">
                    <option>Type Switcher</option>
                    <option value="dvd">DVD</option>
                    <option value="furniture">Furniture</option>
                    <option value="book">Book</option>
                </select>
                <div class="content">
                    <div id="dvd" class="data">
                        <label> <p class="p">Size (MB)</p></label>
                        <input type="number" name="size" id="size" > <br><br>
                        <p>Please provide the capacity in Mega Bytesu(MB) format</p>
                    </div>
                    <div id="furniture" class="data">
                        <label><p class="p">Height (CM)</p></label>
                        <input type="text" name="height" id="height" ><br><br>
                        <label><p class="p">Width (CM)</p></label>
                        <input type="text" name="width" id="width" ><br><br>
                        <label><p class="p">Length (CM)</p></label>
                        <input type="text" name="length" id="length" ><br><br> 
                        <p class="">Please provide the dimensions in HxWxL(CM) format</p>
                    </div>       
                    <div id="book" class="data">
                        <label><p class="p">Weight (KG)</p></label>
                        <input type="text" name="weight" id="weight" ><br><br>
                        <p>Please provide the weight in Kilo Grams(KG) format</p>
                    </div>
                </div>
            </form>
        </form>
        </div>
    </div>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $("#productType").on('change',function(){
                $(".data").hide();
                $("#" + $(this).val()).fadeIn(700);
            }).change();
        });
    </script>
        <footer>
            <?php   include 'footer.php'; ?>
        </footer>
    
</body>
</html>
<?php  
if (isset($_POST['Save'])) {
    mysqli_query($db,"INSERT INTO `products` (`id`, `sku`, `name`, `product_type`, `price`, `size`, `dimensions`, `weight`) VALUES ('', $_POST[sku]', '$_POST[name]', '$_POST[producttype]', '$_POST[price]', 'Size: $_POST[size] MB', 'Dimensions: $_POST[height] x $_POST[width] x $_POST[length]', 'Weight: $_POST[weight]KG');");
       ?> 
        <!--<script type="text/javascript">
          window.location="./index"
        </script>--><?php
}

if (isset($_POST['Cancel'])) {
    ?>
    <script type="text/javascript">
        window.location="./index"
    </script>
    <?php
}
?>